const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");

router.post("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	const data = {
		userId: userData.id,
		reqBody: req.body
	}

	orderController.orderCheckout(data).then(resultFromController => res.send(resultFromController));
})

router.get("/list", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	orderController.getUserOrders(userData.id).then(resultFromController => {
		res.send(resultFromController);
	});
});

module.exports = router;