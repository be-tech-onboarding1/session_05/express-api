const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

router.post("/create", (req, res) => {
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

router.get("/:productId", (req, res) => {	
	productController.getProduct(req.params).then(resultFromController => {
		res.send(resultFromController);
	});
});;

module.exports = router;