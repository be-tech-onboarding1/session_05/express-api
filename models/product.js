const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product Name is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	}
})

module.exports = mongoose.model("Product", productSchema);