const User = require("../models/user");
const auth = require("../auth"); 
const bcrypt = require("bcryptjs");

module.exports.registerUser = (body) => {
	return User.find({email : body.email}).then(result => {
		if (result.length > 0){
			return false; 
		} else {
			let newUser = new User({
				email : body.email,
				password: bcrypt.hashSync(body.password, 10)
			});
		
			return newUser.save().then((user, error) => {
				if (error){
					return false; 
				} else {
					return true; 
				}
			})
		}
	})
}

module.exports.loginUser = (body) => {
	return User.findOne({email : body.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password);

			if(isPasswordCorrect){
				return {access : auth.createAccessToken(result.toObject())}
			} else {
				return false; 
			}
		}
	})
}
