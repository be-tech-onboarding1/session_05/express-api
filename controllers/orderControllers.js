const Order = require("../models/order")
const Product = require("../models/product")
const auth = require("../auth"); 


module.exports.orderCheckout = (data) => {

	return Product.findById(data.reqBody.productId).then(product => {

		if(product == null) {
			return "The product is not available."
		}
		else {

			let newOrder = new Order({
				userId: data.userId,
				products: {
					productName: product.name,
					quantity: data.reqBody.quantity
				},
				totalAmount: product.price * data.reqBody.quantity
			});

			return newOrder.save().then((order, error) => {

				if(error) {
					console.log(error);
					return false;
				}
				else {
					console.log(order)
					return true;
				}
			});
		};
	});
};

module.exports.getUserOrders = (userId) => {

	return Order.find({userId: userId}).then(order => {
		return order;
	});
};
