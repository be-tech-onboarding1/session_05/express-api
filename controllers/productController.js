const Product = require("../models/product");

module.exports.createProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {

		if(error) {
			console.log(error);
			return false;
		} 
		else {
			console.log(product);
			return true;
		}
	});
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(product => {
		return product;
	});
};

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(product => {
		return product;
	});
};