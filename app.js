//Please remember to add your own MongoDB Atlas connection string and order routes
const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

mongoose.connect("mongodb+srv://admin:admin1234@halmontedb.4urphkk.mongodb.net/demo_db?retryWrites=true&w=majority", {
	useNewUrlParser: true,	
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});
